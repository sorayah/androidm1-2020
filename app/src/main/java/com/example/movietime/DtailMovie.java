package com.example.movietime;
import static com.example.movietime.HomeActivity.favorites;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movietime.data.api.RetrofitClient;
import com.example.movietime.data.vo.ActeursList;
import com.example.movietime.data.vo.Video;
import com.example.movietime.data.vo.VideoList;
import com.example.movietime.data.api.Client;
import com.example.movietime.data.vo.Movie;
import com.squareup.picasso.Picasso;

import org.parceler.Parcels;

import java.util.ArrayList;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static  com.example.movietime.ui.popular.PopularFragment.id_MP;
public class DtailMovie extends AppCompatActivity {
    private TextView movieTitle;
    private String title;

    private ImageView image;
    private String picture;
    private TextView Overview;
    private String over;
    private TextView av;
    private Double aver;
    private int movie_id;
    private TextView  VoteC;
    private TextView dateS;
    RecyclerView recyclerView;
    ActeursAdapter recyclerAdapter;
    Movie movie;
    private ImageButton add;
    private  ImageButton remove;
    public ArrayList<Video> videos = new ArrayList<Video>();
    public WebView videoYoutube;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dtail_movie);
        movieTitle = findViewById(R.id.movieTitle);
        av = findViewById(R.id.movie_tagline);
        image = findViewById(R.id.moviePoster);
        Overview = findViewById(R.id.movie_overview);
        VoteC=findViewById(R.id.movie_rating);
        dateS=findViewById(R.id.movie_release_date);
        videoYoutube=findViewById(R.id.video);
        add = (ImageButton) findViewById(R.id.favorite);
        remove=(ImageButton) findViewById(R.id.remove);
        remove.setVisibility(View.INVISIBLE);
        recyclerView =findViewById(R.id.recyclerViewActeurs);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);

        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerAdapter = new ActeursAdapter(new ArrayList() , getApplicationContext());




        if(getIntent().hasExtra(String.valueOf(id_MP))){
            Parcelable p = getIntent().getParcelableExtra(String.valueOf(id_MP));
            movie=new Movie();
            movie= Parcels.unwrap(p);
            displayData(movie);
            Log.e("hah","hah"+movie.getTitle()+movie.getVoteAverage()+movie.getPoster()+movie.getId());
        }else{
            Toast.makeText(this, "No API Data", Toast.LENGTH_SHORT).show();
        }
        refreshContent(movie);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFavorite(movie);
                refreshContent(movie);
            }
        });


        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFavorite(movie);
                refreshContent(movie);
            }
        });
        fetchActeurs(recyclerAdapter,movie.getId());
        recyclerView.setAdapter(recyclerAdapter);
        fetchVideo(movie.getId());


    }

    public void fetchActeurs(final ActeursAdapter recyclerAdapter , int movieId) {
        Client movieService = RetrofitClient.getInstance().create(Client.class);
        movieService.getMovieActeurs(movieId,"1abe855bc465dce9287da07b08a664eb").enqueue(new Callback<ActeursList>() {

            @Override
            public void onResponse(Call<ActeursList> call, Response<ActeursList> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage data
                    ActeursList collection = response.body();
                    Log.e("hahah",""+collection.getActeursList().get(1).getName());
                    recyclerAdapter.addFeatureList(collection.getActeursList());
                    } else {
                        Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                    }
                    }

                    @Override
                    public void onFailure(Call<ActeursList> call, Throwable t) {
                        //Manage errors
                    }
                });
    }
    public void fetchVideo( int movieId) {
        Client movieService = RetrofitClient.getInstance().create(Client.class);
        movieService.getVideo(movieId,"1abe855bc465dce9287da07b08a664eb").enqueue(new Callback<VideoList>() {

            @Override
            public void onResponse(Call<VideoList> call, Response<VideoList> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage data
                    VideoList collection = response.body();
                    videos.addAll(collection.getResults()) ;
                    String pvy="";
                    for(int i = 0; i< collection.getResults().size(); i++) {
                        if(!collection.getResults().get(i).getKey().equals("") ) {
                            pvy = collection.getResults().get(i).getKey();
                            i = collection.getResults().size();
                        }
                    }
                    videoYoutube.getSettings().setJavaScriptEnabled(true);
                    videoYoutube.setWebChromeClient(new WebChromeClient());
                    videoYoutube.loadData(pvy, "text/html", "utf-8");

                } else {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<VideoList> call, Throwable t) {
                //Manage errors
            }
        });
    }




    private void displayData(Movie movie) {
        movieTitle.setText(movie.getTitle());
        title=movie.getTitle();
        av.setText(""+movie.getVoteAcount());
        aver=movie.getVoteAverage();
        Overview.setText(movie.getOverview());
        over=movie.getOverview();
        picture=movie.getPoster_path();
        VoteC.setText(""+movie.getVoteAverage());
        dateS.setText(movie.getrelease_date());
        Picasso.with( getApplicationContext() )
                .load("https://image.tmdb.org/t/p/w500"+picture)
                .placeholder(R.mipmap.logo3)
                .error(R.drawable.no_image)
                .into(image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });

       // image.setText(feature.getProperties().getPostalCode() + " - " + feature.getProperties().getCity());
    }
    public void saveFavorite(Movie movie){
            favorites.add(movie);
        Toast.makeText(getApplicationContext(), "Film "+movie.getTitle()+" a été ajouté aux favoris", Toast.LENGTH_LONG).show();
        refreshContent(movie);
    }
    public void removeFavorite(Movie movie){
        ArrayList<Movie> favDelet = new ArrayList<Movie>();
        for (int i=0;i<favorites.size();i++){
            if(favorites.get(i).getId()== movie.getId()){
                favorites.remove(i);
                favDelet.addAll(favorites);
                favorites.clear();
                favorites.addAll(favDelet);
                Toast.makeText(getApplicationContext(), "Film "+movie.getTitle()+" a été supprimé des favoris", Toast.LENGTH_LONG).show();
                refreshContent(movie);
            }
        }
    }
    public Boolean containsMovie(Movie movie){
        for (int i=0;i<favorites.size();i++){
            if(favorites.get(i).getId()== movie.getId()){
                return true;
            }
        }
        return false;
    }

    public void refreshContent(Movie movie) {
        if(!containsMovie(movie)) {
            add.setVisibility(View.VISIBLE);
            remove.setVisibility(View.INVISIBLE);
        } else {
            add.setVisibility(View.INVISIBLE);
            remove.setVisibility(View.VISIBLE);
        }
        refresh(movie);
    }


    public void refresh(final Movie movie) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refreshContent(movie);
            }
        };

        handler.postDelayed(runnable, 1000);
    }



}
