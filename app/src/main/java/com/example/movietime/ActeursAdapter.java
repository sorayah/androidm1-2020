package com.example.movietime;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movietime.data.vo.Acteurs;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ActeursAdapter extends RecyclerView.Adapter<ActeursAdapter.ViewHolder> {
    private Context context;
    private ArrayList<Acteurs> acteurs;
    public ActeursAdapter(ArrayList<Acteurs> acteurs, Context context) {
        this.acteurs=acteurs;
        this.context = context;
    }
    @Override
    public int getItemCount() {
        return acteurs.size();
    }
    public void addFeatureList(ArrayList<Acteurs> acteurs) {
        this.acteurs.clear();
        this.acteurs.addAll(acteurs);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.acteurs, parent, false);
        Log.e("TAaG", "onCreateViewHolder");
        return new ActeursAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Acteurs acteur =this.acteurs.get(position);
        holder.acteurName.setText(acteur.getName());
        Picasso.with(context)
                .load("https://image.tmdb.org/t/p/w500"+acteur.getProfile_path())
                .placeholder(R.mipmap.logo3)
                .error(R.drawable.no_image)
                .into(holder.acteurPicture);
    }
    class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        ImageView acteurPicture;
        TextView acteurName;
        ViewHolder(View view) {
            super(view);
            itemView = view;
            acteurPicture= view.findViewById(R.id.acteurPicture);
            acteurName= view.findViewById(R.id.acteurName);
        }

    }



}
