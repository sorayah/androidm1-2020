package com.example.movietime;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.movietime.data.vo.Movie;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.movietime.HomeActivity.favorites;

public class RecyclerViewAdapter  extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>  {

    private List<Movie> MovieList;
    private OnItemClickListener listener;
    Context context;
    private String urlImage;

    public interface OnItemClickListener {
        void onItemClick(View view, Movie Movie);
    }

    public RecyclerViewAdapter(List<Movie> MovieList, OnItemClickListener listener, Context context) {
        this.MovieList = MovieList;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return this.MovieList.size();
    }

    public void addMovieList(List<Movie> MovieList) {
        this.MovieList.addAll(MovieList);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new RecyclerViewAdapter.ViewHolder(view);
    }
    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Movie Movie = MovieList.get(position);

        //holder.image.setImageResource(Movie.getPoster());
        holder.MovieName.setText(Movie.getTitle());
        holder.date.setText(""+Movie.getrelease_date());
        String urlPoster ="https://image.tmdb.org/t/p/w500"+ Movie.getPoster();
        Log.i("Poster",Movie.getPoster()+Movie.getTitle()+Movie.getId());
        loadImageFromURL(holder,urlPoster);
        /*this.urlImage=Movie.getPoster();
        loadImageFromURL(holder,urlPoster);
        holder.image.setImageDrawable(LoadImageFromWebOperations(Movie.getPoster()));*/

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(v, Movie);
            }
        });

        refreshContent(holder, Movie);

        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFavorite(holder ,Movie);
                refreshContent(holder ,Movie);
            }
        });


        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFavorite(holder ,Movie);
                refreshContent(holder ,Movie);
            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View itemView;
        final TextView MovieName;
        final TextView date;
        final ImageView image;
        final ImageButton add;
        final  ImageButton remove;

        ViewHolder(View view) {
            super(view);
            itemView = view;
            MovieName = view.findViewById(R.id.MovieName);
            date = view.findViewById(R.id.DateS);
            image = view.findViewById(R.id.MovieImage);
            add = (ImageButton) view.findViewById(R.id.favorite);
            remove=(ImageButton) view.findViewById(R.id.remove);
        }
    }



    private void loadImageFromURL(ViewHolder holder,String url) {
        Picasso.with( context )
                .load(url)
                .placeholder(R.mipmap.logo3)
                .error(R.drawable.no_image)
                .into(holder.image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    public void saveFavorite(final ViewHolder holder ,Movie movie){
        favorites.add(movie);
        Toast.makeText(context, "Film "+movie.getTitle()+" a été ajouté aux favoris", Toast.LENGTH_LONG).show();

        refreshContent(holder,movie);
    }
    public void removeFavorite(final ViewHolder holder,Movie movie){
        ArrayList<Movie> favDelet = new ArrayList<Movie>();
        for (int i=0;i<favorites.size();i++){
            if(favorites.get(i).getId()== movie.getId()){
                favorites.remove(i);
                favDelet.addAll(favorites);
                favorites.clear();
                favorites.addAll(favDelet);

                Toast.makeText(context, "Film "+movie.getTitle()+" a été supprimé des favoris", Toast.LENGTH_LONG).show();
                refreshContent(holder ,movie);
            }
        }
    }

    public Boolean containsMovie(Movie movie){
        for (int i=0;i<favorites.size();i++){
            if(favorites.get(i).getId()== movie.getId()){
                return true;
            }
        }
        return false;
    }

    public void refreshContent(final ViewHolder holder,Movie movie) {
        if(!containsMovie(movie)) {
            holder.add.setVisibility(View.VISIBLE);
            holder.remove.setVisibility(View.INVISIBLE);
        } else {
            holder.add.setVisibility(View.INVISIBLE);
            holder.remove.setVisibility(View.VISIBLE);
        }
        refresh(holder,movie);
    }


    public void refresh(final ViewHolder holder ,final Movie movie) {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                refreshContent(holder,movie);
            }
        };

        handler.postDelayed(runnable, 1000);
    }

}