package com.example.movietime.ui.Favoris;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movietime.DtailMovie;
import com.example.movietime.R;
import com.example.movietime.RecyclerViewAdapter;
import com.example.movietime.data.vo.Movie;
import com.example.movietime.ui.popular.PopularViewModel;

import org.parceler.Parcels;

import java.util.ArrayList;
import static com.example.movietime.HomeActivity.favorites;
public class FavorisFragment extends Fragment {

    private FavorisViewModel sendViewModel;
    private PopularViewModel popularViewModel;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    public static final int id_MP=0;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        //popularViewModel = ViewModelProviders.of(this).get(PopularViewModel.class);
        View root = inflater.inflate(R.layout.fragment_popular, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
       /* popularViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/

        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        //Initialize layoutManager with a LinearLayoutManager, by default vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        //Add list divider between each line
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        //Create adapter with the OnItemClickListener implementation

        recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Movie movie) {
                Intent intent = new Intent(getContext(), DtailMovie.class);
                intent.putExtra(String.valueOf(id_MP), Parcels.wrap(movie) );
                startActivity(intent);
            }
        },getContext());




        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);

        if (!favorites.isEmpty()){
            recyclerAdapter.addMovieList(favorites);
        }else{
            Toast.makeText(getContext(), "Votre liste est vide", Toast.LENGTH_LONG).show();
        }
        return root;
    }

}