package com.example.movietime.ui.Upcoming;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movietime.DtailMovie;
import com.example.movietime.R;
import com.example.movietime.RecyclerViewAdapter;
import com.example.movietime.data.api.RetrofitClient;
import com.example.movietime.data.api.Client;
import com.example.movietime.data.vo.Movie;
import com.example.movietime.data.vo.MovieList;

import org.parceler.Parcels;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static  com.example.movietime.ui.popular.PopularFragment.id_MP;
public class UpcomingFragment extends Fragment {




    private UpcomingViewModel UpcomingViewModel;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_popular, container, false);
        final TextView textV = root.findViewById(R.id.text_home);


        recyclerView = (RecyclerView) root.findViewById(R.id.recyclerView);
        //Initialize layoutManager with a LinearLayoutManager, by default vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        //Add list divider between each line
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        //Create adapter with the OnItemClickListener implementation
        recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Movie movie) {
                Intent intent = new Intent(getContext(), DtailMovie.class);
                intent.putExtra(String.valueOf(id_MP), Parcels.wrap(movie) );
                startActivity(intent);
            }
        },getContext());

        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);

        fetchMetromobiliteData();
        return root;
    }

    private void fetchMetromobiliteData() {
        Client client = RetrofitClient.getInstance().create(Client.class);
        client.getUpcomingMovieList("1abe855bc465dce9287da07b08a664eb").enqueue(new Callback<MovieList>() {

            @Override
            public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage data
                    MovieList collection = response.body();
                    recyclerAdapter.addMovieList(collection.getMovieList());
                } else {
                    Toast.makeText(getContext(), "error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MovieList> call, Throwable t) {
                //Manage errors
            }
        });
    }


}