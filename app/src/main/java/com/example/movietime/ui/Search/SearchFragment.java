package com.example.movietime.ui.Search;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import android.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movietime.DtailMovie;
import com.example.movietime.R;
import com.example.movietime.RecyclerViewAdapter;
import com.example.movietime.data.api.RetrofitClient;
import com.example.movietime.data.api.Client;
import com.example.movietime.data.vo.Movie;
import com.example.movietime.data.vo.MovieList;


import org.parceler.Parcels;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {


    private SearchViewModel SearchViewModel;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    public static final int id_MP=0;
    public TextView textView;
    public SearchView search;
    public ArrayList<Movie> movies;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search, container, false);
        textView = root.findViewById(R.id.noData);
        search= (SearchView) root.findViewById(R.id.search_bar);

        recyclerView = (RecyclerView) root.findViewById(R.id.searchView);
        //Initialize layoutManager with a LinearLayoutManager, by default vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        //Add list divider between each line
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        //Create adapter with the OnItemClickListener implementation
        movies= new ArrayList<>();
        recyclerAdapter = new RecyclerViewAdapter(movies, new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Movie movie) {
                Intent intent = new Intent(getContext(), DtailMovie.class);
                intent.putExtra(String.valueOf(id_MP), Parcels.wrap(movie) );
                startActivity(intent);
            }
        },getContext());

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {
                fetchShearchMovie(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                movies.clear();
                fetchShearchMovie(query);
                return false;
            }

        });
        if(!search.hasFocus()) {
            textView.setText("Aucun resultat");
        }



        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);


        return root;
    }

    private void fetchShearchMovie(String query) {
        Client client = RetrofitClient.getInstance().create(Client.class);
        client.getSearchMovie(query,"1abe855bc465dce9287da07b08a664eb").enqueue(new Callback<MovieList>() {

            @Override
            public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                if(response.isSuccessful() && response.body() != null) {
                    //Manage data
                    MovieList collection = response.body();
                    recyclerAdapter.addMovieList(collection.getMovieList());
                } else {
                    Toast.makeText(getContext(), "Pas de films", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<MovieList> call, Throwable t) {
                //Manage errors
            }
        });


    }




}