package com.example.movietime.ui.topRated;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class topRatedViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public topRatedViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Top Rated Movies Fragement");
    }

    public LiveData<String> getText() {
        return mText;
    }
}