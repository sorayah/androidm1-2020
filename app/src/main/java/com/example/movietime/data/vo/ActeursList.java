package com.example.movietime.data.vo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ActeursList {
    @SerializedName("cast")
    private ArrayList<Acteurs> ActeursList;

    public ActeursList(ArrayList<Acteurs> ActeursList){
        this.ActeursList=ActeursList;
    }

    public ArrayList<Acteurs> getActeursList() {
        return ActeursList;
    }

    public void setActeursList(ArrayList<Acteurs> acteursList) {
        ActeursList = acteursList;
    }
}
