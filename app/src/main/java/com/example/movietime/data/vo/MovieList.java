package com.example.movietime.data.vo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class MovieList {

    @SerializedName("results")
    private ArrayList<Movie> MovieList;
    public MovieList(ArrayList<Movie> MovieList){
        this.MovieList=MovieList;
    }

    public ArrayList<Movie> getMovieList() {
        return MovieList;
    }

    public void setMovieList(ArrayList<Movie> MovieList) {
        this.MovieList = MovieList;
    }

}









