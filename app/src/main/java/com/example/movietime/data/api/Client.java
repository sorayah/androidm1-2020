package com.example.movietime.data.api;

import com.example.movietime.data.vo.ActeursList;
import com.example.movietime.data.vo.MovieList;
import com.example.movietime.data.vo.VideoList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
public interface Client {

    public static final String API_KEY = "1abe855bc465dce9287da07b08a664eb";
    @GET("/3/movie/popular")
    Call<MovieList> getPopularMovieList(@Query("api_key") String api);

    @GET("/3/movie")
    Call<MovieList> getMovie(@Query("movie_id") int id, @Query("api_key") String api);


    @GET("/3/movie/top_rated")
    Call<MovieList> getTopRatedMovieList(@Query("api_key") String api);

    @GET("/3/movie/upcoming")
    Call<MovieList> getUpcomingMovieList(@Query("api_key") String api);

    @GET("/3/movie/now_playing")
    Call<MovieList> getNowPlayingMovieList(@Query("api_key") String api);

    @GET("/3/movie/{movie_id}/credits")
    Call<ActeursList> getMovieActeurs(@Path("movie_id") int id, @Query("api_key") String api_key);

    @GET("/3/search/movie")
    Call<MovieList> getSearchMovie(@Query("query") String query, @Query("api_key") String api);


    @GET("/3/movie/{movie_id}/videos")
    Call<VideoList> getVideo(@Path("movie_id") int id, @Query("api_key") String api);


}

