package com.example.movietime.data.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@org.parceler.Parcel
public class Movie implements Parcelable {
    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("overview")
    public String overview;

    @SerializedName("release_date")
    public String release_date;

    @SerializedName("popularity")
    public double popularity;

    @SerializedName("vote_count")
    public int vote_count;

    @SerializedName("video")
    public Boolean video;

    @SerializedName("backdrop_path")
    public String backdrop_path;

    @SerializedName("vote_average")
    public double vote_average;

    @SerializedName("poster_path")
    public String poster_path;


    public Movie(int Id,String poster, String Title,String overview,String release_date,double Popularity,int vote_count,Boolean Video,
                 double vote_average , String poster_path ) {

        this.id=Id;
        this.title=Title;
        this.overview=overview;
        this.release_date=release_date;
        this.popularity=Popularity;
        this.vote_count=vote_count;
        this.video=Video;
        this.vote_average=vote_average;
        this.backdrop_path=poster;
        this.poster_path=poster_path;
    }
    public Movie(){
        this.id=0;
        this.title="Title";
        this.overview="overview";
        this.release_date="release_date";
        this.popularity=0.0;
        this.vote_count=0;
        this.video=true;
        this.vote_average=0.0;
        this.backdrop_path="poster";
        this.poster_path="";
    }

    protected Movie(Parcel in) {
        id = in.readInt();
        title = in.readString();
        overview = in.readString();
        release_date = in.readString();
        popularity = in.readDouble();
        vote_count = in.readInt();
        byte tmpVideo = in.readByte();
        video = tmpVideo == 0 ? null : tmpVideo == 1;
        backdrop_path = in.readString();
        poster_path=in.readString();
        vote_average = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(release_date);
        dest.writeDouble(popularity);
        dest.writeInt(vote_count);
        dest.writeByte((byte) (video == null ? 0 : video ? 1 : 2));
        dest.writeString(backdrop_path);
        dest.writeString(poster_path);
        dest.writeDouble(vote_average);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getPoster() {
        return backdrop_path;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public void setPoster(String poster) {
        this.backdrop_path = poster;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        overview = overview;
    }

    public String getrelease_date() {
        return release_date;
    }

    public void setrelease_date(String release_date) {
        this.release_date = release_date;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(int popularity) {
        this.popularity = popularity;
    }

    public int getVoteAcount() {
        return vote_count;
    }

    public void setvote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public Boolean getVideo() {
        return video;
    }

    public void setVideo(Boolean video) {
        this.video = video;
    }

    public double getVoteAverage() {
        return vote_average;
    }

    public void setVoteAverage(double vote_average) {
        this.vote_average = vote_average;
    }
}
