package com.example.movietime;
import static com.example.movietime.HomeActivity.favorites;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.movietime.data.api.Client;
import com.example.movietime.data.vo.Movie;
import com.example.movietime.data.vo.MovieList;
import com.example.movietime.ui.popular.PopularViewModel;

import org.parceler.Parcels;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Favorite extends AppCompatActivity {


    private PopularViewModel popularViewModel;
    RecyclerView recyclerView;
    RecyclerViewAdapter recyclerAdapter;
    public static final int id_MP=0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        recyclerView = findViewById(R.id.recyclerView);
        //Initialize layoutManager with a LinearLayoutManager, by default vertical
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Add list divider between each line
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayout.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        //Create adapter with the OnItemClickListener implementation

        recyclerAdapter = new RecyclerViewAdapter(new ArrayList(), new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Movie movie) {
                Intent intent = new Intent(getApplicationContext(), DtailMovie.class);
                intent.putExtra(String.valueOf(id_MP), Parcels.wrap(movie) );
                startActivity(intent);
            }
        },getApplicationContext());




        //Set the adapter
        recyclerView.setAdapter(recyclerAdapter);
        if (!favorites.isEmpty()){
            recyclerAdapter.addMovieList(favorites);
        }else{
            Toast.makeText(getApplicationContext(), "Votre liste est vide", Toast.LENGTH_LONG).show();
        }

    }





}
